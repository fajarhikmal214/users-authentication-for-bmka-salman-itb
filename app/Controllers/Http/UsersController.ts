import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/users'

export default class UsersController {

    public async register({ request, response }: HttpContextContract ) {

        const validationSchema = schema.create({
            username: schema.string({ trim: true }, [
                rules.unique({ table: 'users', column: 'username' }),
            ]),
            password: schema.string({}, [
                rules.confirmed(),
            ]),
            email: schema.string({ trim: true }, [
                rules.email(),
                rules.unique({ table: 'users', column: 'email' }),
            ]),
            phone: schema.string({ trim: true }, []),
        })

        try {

            const userDetails = await request.validate({
                schema: validationSchema,
            })

            const user = new User()
            user.username = userDetails.username
            user.password = userDetails.password
            user.email = userDetails.email
            user.phone = userDetails.phone 
            const data = await user.save()

            return response
                .status(201)
                .json({
                    'message': 'Your account has been created',
                    'status': true,
                    'data': data
                })
        } catch (error) {
            return response
                .status(400)
                .json({
                    'message': 'Your account has failed to create',
                    'status': false,
                    'errors': error.messages
                })
        }
    }

    public async login({ auth, request, response }: HttpContextContract) {

        const validationSchema = schema.create({
            username: schema.string(),
            password: schema.string(),
        })

        try {        

            const userDetails = await request.validate({
                schema: validationSchema,
            })

            const data = await auth.use('api').attempt(userDetails.username, userDetails.password)
            const user = await User.findBy('username', userDetails.username)

            return response
                .status(200)
                .json({
                    'message': 'You have successfully logged in',
                    'status': true,
                    'user': user,
                    'data': data.toJSON()
                })
        } catch (error) {
            return response    
                .status(400)
                .json({
                    'message': 'Your username or password is incorrect',
                    'status': false,
                    'errors': error.messages
                })
        }
    }
}
